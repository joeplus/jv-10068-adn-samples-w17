﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week8
{
    class Drums : Instrument
    {
        public override void Tune()
        {
            Console.WriteLine("Your drums have been tuned!");
        }

        public static bool operator== (Drums lhs, Drums rhs){
             return true;
        }

        public static bool operator!=(Drums lhs, Drums rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Drums))
                return false;

            return this == (Drums)obj;
        }
    }
}
