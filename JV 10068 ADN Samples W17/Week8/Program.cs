﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week8
{
    class Program
    {
        static void Main(string[] args)
        {
            InstrumentTester();
        }

        private static void InstrumentTester()
        {
            Instrument[] instruments = { new SnareDrums(),
                                         new Guitar(),
                                         new Drums(),
                                         new Guitar(),
                                         new SnareDrums()
                                        };

            foreach (Instrument instrument in instruments)
            {
                Console.WriteLine(instrument);
                instrument.Play();
                instrument.Tune();
            }
        }
    }
}
