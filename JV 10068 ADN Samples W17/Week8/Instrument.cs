﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week8
{
    abstract class Instrument
    {
        public virtual void Play() { Console.WriteLine("Instrument.Play"); }
        public abstract void Tune();
    }
}
