﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week8
{
    class Guitar : Instrument
    {
        public override void Tune()
        {
            Console.WriteLine("Your guitar has been tuned!");
        }

        public override void Play()
        {
            Console.WriteLine("Your guitar is now playing");
        }

        public override string ToString()
        {
            return "This is a Guitar object";
        }
    }
}
