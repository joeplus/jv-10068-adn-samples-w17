﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Week11
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void SubmitBtn_Click(object sender, RoutedEventArgs e)
        {
            GreetlingLbl.Content = "Hello WPF World!";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            decimal subtotal, taxRate, tax, total;

            if (!decimal.TryParse(AmountTB.Text, out subtotal))
            {
                MessageBox.Show("Invalid Amount Entered!");
                return;
            }

            if (!decimal.TryParse(TaxTB.Text, out taxRate))
            {
                MessageBox.Show("Invalid Tax Rate Entered!");
                return;
            }

            tax = subtotal * taxRate / 100;
            total = subtotal + tax;

            TaxLbl.Content = tax.ToString("C");
            TotalLbl.Content = total.ToString("C");
        }
    }
}
