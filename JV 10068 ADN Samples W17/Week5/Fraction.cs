﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week5
{
    class Fraction
    {
        public int Numerator { get; set; }
        public int Denominator { get; set; }

        public Fraction(int numerator, int denominator)
        {
            Numerator = numerator;
            Denominator = denominator;
        }

        public static Fraction operator +(Fraction lhs, Fraction rhs)
        {
            if (lhs.Denominator == rhs.Denominator)
                return new Fraction(lhs.Numerator + rhs.Numerator, rhs.Denominator);
            else
            {
                int numerator1 = lhs.Numerator * rhs.Denominator;
                int numerator2 = rhs.Numerator * lhs.Denominator;
                return new Fraction(numerator1 + numerator2, lhs.Denominator * rhs.Denominator);
            }
        }

        public static bool operator ==(Fraction lhs, Fraction rhs)
        {
            if ((lhs.Numerator == rhs.Numerator) && (lhs.Denominator == rhs.Denominator))
                return true;
            else
                return false;
        }
        public static bool operator !=(Fraction lhs, Fraction rhs)
        {
            return !(lhs == rhs);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Fraction))
                return false;

            return this == (Fraction)obj;
        }

        public static implicit operator Fraction(int number)
        {
            return new Fraction(number, 1);
        }

        public static explicit operator int(Fraction fraction)
        {
            return fraction.Numerator / fraction.Denominator;
        }
    }
}
