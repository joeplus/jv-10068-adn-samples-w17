﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week5
{
    class Program
    {
        static void Main(string[] args)
        {
            FractionTester();
        }

        private static void FractionTester()
        {
            Fraction fraction1 = new Fraction(1, 2);
            Fraction fraction2 = new Fraction(2, 4);
            Fraction result = fraction1 + fraction2;

            Fraction fraction3 = new Fraction(1, 2);
            bool operatorEqualsResult = (fraction1 == fraction3);
            bool equalsMethodResult = fraction1.Equals(fraction3);
            bool operatorNotEqualsResult = (fraction1 != fraction2);

            int value = 2;
            Fraction fraction4 = value;
            int intResult = (int)result;
        }
    }
}
