﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week3
{
    class Program
    {
        static void Main(string[] args)
        {
            MathTester();
            StaticMathTester();
            DogTester();
        }

        private static void DogTester()
        {
            //Milo, 26 pounds; Frisky, 10 pounds; and Laika, 50 pounds
            Dog[] myDogs = {new Dog("Milo", 26),
                            new Dog("Frisky", 10),
                            new Dog("Laika", 50),
                            new Dog()};

            //for (int i = 0; i < myDogs.Length; i++)
            //    Console.WriteLine(myDogs[i]);

            foreach (Dog dog in myDogs)
                Console.WriteLine(dog);
        }

        private static void StaticMathTester()
        {
            int result = StaticMath.Add(10, 2);
            result = StaticMath.Subtract(10, 2);
            result = StaticMath.Multiply(10, 2);
            result = StaticMath.Divide(10, 2);
        }

        private static void MathTester()
        {
            Math myMath = new Math();
            int result = myMath.Add(10, 2);
            result = myMath.Subtract(10, 2);
            result = myMath.Multiply(10, 2);
            result = myMath.Divide(10, 2);
        }
    }
}
