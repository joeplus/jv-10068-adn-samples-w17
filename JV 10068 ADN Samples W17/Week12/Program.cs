﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week12
{
    class Program
    {
        static void Main(string[] args)
        {
            //CustomersTable();
            //ProductsTable();
            //ProductsTable2();
            LINQBookCollection();
            LINQBookAndOrderCollections();
        }

        private static void LINQBookAndOrderCollections()
        {
            throw new NotImplementedException();
        }

        private static void LINQBookCollection()
        {
            Book[] myBooks = { new Book {Title = "C# 4.0 In a Nutshell", Author = "Joseph Albahari", Year = 2010},
                               new Book {Title = "C# 5.0 In a Nutshell", Author = "Joseph Albahari", Year = 2014},
                               new Book {Title = "C# 6.0 In a Nutshell", Author = "Joseph Albahari", Year = 2016},     
                               new Book {Title = "Lord of the Rings", Author = "J.R. Tolkien", Year = 1940},
                               new Book {Title = "Lord of the Rings Two Towers", Author = "J.R. Tolkien", Year = 1995}
                             };

            IEnumerable<Book> csBooks = from aBook in myBooks
                                        where aBook.Title.Contains("C#")
                                        orderby aBook.Year
                                        select aBook;
            foreach (Book book in csBooks)
                Console.WriteLine(book);

            Console.WriteLine("\nLINQ using projection...");
            var csProjectedBooks = from aBook in myBooks
                                   where aBook.Title.Contains("C#")
                                   orderby aBook.Year
                                   select new { aBook.Title, aBook.Year };

            foreach (var book in csProjectedBooks)
                Console.WriteLine(book);

            // using Lambda expressions
            Console.WriteLine("\nLINQ using projection and Lambda syntax...");
            csProjectedBooks = myBooks.Where(b => b.Title.Contains("C#"))
                                      .OrderBy(b => b.Year)
                                      .Select(b => new { b.Title, b.Year });

            foreach (var book in csProjectedBooks)
                Console.WriteLine(book);

            Order[] myOrders = {new Order {Title ="C# 4.0 In a Nutshell", Quantity = 50},
                                new Order {Title ="C# 5.0 In a Nutshell", Quantity = 60},
                                new Order {Title ="C# 6.0 In a Nutshell", Quantity = 70},
                                new Order {Title ="Lord of the Rings", Quantity = 70},
                               };

            Console.WriteLine("\nLINQ using projection and a join...");
            var csProjectedOrders = from aBook in myBooks
                                    join anOrder in myOrders on aBook.Title equals anOrder.Title
                                    where aBook.Title.Contains("C#") || anOrder.Quantity > 50
                                    orderby aBook.Year descending
                                    select new { aBook.Title, aBook.Year, anOrder.Quantity };
            foreach (var order in csProjectedOrders)
                Console.WriteLine(order);
        }

        private static void ProductsTable()
        {
            string conStr = "server = .\\sqlexpress; Database = Northwind; Trusted_Connection = yes";
            string cmdStr = "SELECT ProductID, ProductName, UnitsInStock FROM Products WHERE UnitsInStock < 10";

            try
            {
                SqlDataAdapter productsDA = new SqlDataAdapter(cmdStr, conStr);
                DataSet productsDS = new DataSet();
                productsDA.Fill(productsDS);
                DataTable productTbl = productsDS.Tables[0];

                foreach (DataRow row in productTbl.Rows)
                {
                    Console.WriteLine("ProductID: {0}, ProductName: {1}, UnitsInStock: {2}",
                                        row["ProductID"], row["ProductName"], row["UnitsInStock"]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private static void ProductsTable2()
        {
            string conStr = "server = .\\sqlexpress; Database = Northwind; Trusted_Connection = yes";
            string cmdStr = "SELECT ProductID, ProductName, UnitsInStock FROM Products";

            try
            {
                SqlDataAdapter productsDA = new SqlDataAdapter(cmdStr, conStr);
                DataSet productsDS = new DataSet();
                productsDA.Fill(productsDS);
                DataTable productTbl = productsDS.Tables[0];

                Console.WriteLine("*******************");
                foreach (DataRow row in productTbl.Rows)
                {
                    int stock;
                    bool result = int.TryParse(row["UnitsInStock"].ToString(), out stock);
                    if (stock < 10)
                        Console.WriteLine("ProductID: {0}, ProductName: {1}, UnitsInStock: {2}",
                                            row["ProductID"], row["ProductName"], row["UnitsInStock"]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
        private static void CustomersTable()
        {
            string conStr = "server = .\\sqlexpress; Database = Northwind; Trusted_Connection = yes";
            string cmdStr = "SELECT CompanyName, ContactName FROM Customers";

            try
            {
                SqlDataAdapter customersDA = new SqlDataAdapter(cmdStr, conStr);
                DataSet customersDS = new DataSet();
                customersDA.Fill(customersDS);
                DataTable customersTbl = customersDS.Tables[0];

                foreach (DataRow row in customersTbl.Rows)
                {
                    Console.WriteLine("CompanyName: {0}; ContactName: {1}", row["CompanyName"], row["ContactName"]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
