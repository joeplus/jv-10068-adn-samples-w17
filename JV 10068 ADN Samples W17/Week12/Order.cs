﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week12
{
    class Order
    {
        public string Title { get; set; }
        public int Quantity { get; set; }
    }
}
