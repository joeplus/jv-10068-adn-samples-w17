﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2
{
    class BankAccount
    {
        private decimal balance;

        /// <summary>
        /// Passing a value type (decimal) by value.  Passing by value is the default for most
        /// languages including C# since it is the "safest" approach as the caller's variable
        /// can NOT be affected.  Pass by value means that a copy of the value is created in
        /// the called code.
        /// </summary>
        /// <param name="amount"></param>
        public void Deposit(decimal amount)
        {
            balance += amount;
            amount = 0;
        }

        /// <summary>
        /// Passing a value type (decimal) by reference so that it can be altered in the caller's
        /// code.
        /// </summary>
        /// <param name="amount"></param>
        public void Deposit(ref decimal amount)
        {
            balance += amount;
            amount = 0;
        }

        /// <summary>
        /// Passing a value type (decimal) by reference using the "out" keyword meaning that it must
        /// be updated in this method which will also update the value in the caller's code.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="total"></param>
        public void Deposit(decimal amount, out decimal total)
        {
            balance += amount;
            total = balance;
        }

        /// <summary>
        /// Passing a reference type (array) by value.  This will allow the callers array to be
        /// updated but the caller's array variable can NOT be altered to reference a different array in 
        /// memory.  This is the default behaviour in most languages for reference types.
        /// </summary>
        /// <param name="transactions"></param>
        public void PostTransactions(decimal[] transactions)
        {
            foreach (decimal transaction in transactions)
                balance += transaction;

            for (int i = 0; i < transactions.Length; i++)
                transactions[i] += 1;

            transactions = new decimal[] { 0, 100, 200 };
        }

        /// <summary>
        /// Passing a reference type (array) by reference.  This will allow the callers array to be
        /// updated and the caller's array variable can be altered to reference a different array in memory.
        /// </summary>
        /// <param name="transactions"></param>
        public void PostTransactions(ref decimal[] transactions)
        {
            foreach (decimal transaction in transactions)
                balance += transaction;

            for (int i = 0; i < transactions.Length; i++)
                transactions[i] += 1;

            transactions = new decimal[] { 0, 100, 200 };
        }
    }
}
