﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4
{
    class Plane : Vehicle
    {
        public void Drive()
        {
            Console.WriteLine("Plane:Drive");
        }

        public override void Turn()
        {
            Console.WriteLine("Plane:Turn");
        }

        public override void Maintain()
        {
            Console.WriteLine("Plane:Maintain");
        }
    }
}
