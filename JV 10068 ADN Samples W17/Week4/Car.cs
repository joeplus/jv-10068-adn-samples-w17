﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4
{
    class Car : Vehicle
    {
        public void Drive()
        {
            Console.WriteLine("Car:Drive");
        }

        public override void Decelerate()
        {
            Console.WriteLine("Car:Decelerate");
        }

        public bool CarOnlyMethod() { return true; }

        public override void Turn()
        {
            Console.WriteLine("Car:Turn");
        }

        public override void Maintain()
        {
            Console.WriteLine("Car:Maintain");
        }
    }
}
