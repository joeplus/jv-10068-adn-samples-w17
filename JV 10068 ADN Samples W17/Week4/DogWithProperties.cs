﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4
{
    class DogWithProperties
    {
        private int weight;
        //private string name;
        //private string color;

        // automatic property with initializer
        public string Name { get; } = "Undefined";
        public string Color { get; set; } = "Undefined";

        // manual property
        public int Weight {
            get { return weight; }
            set {
                if ((weight > 0) && (weight < 500))
                    weight = value;
            }
        }


        //public int GetWeight() { return weight; }
        //public void SetWeight(int weight) { this.weight = weight; }

        //public string GetName() { return name; }
        //public void SetName(string name) { this.name = name; }

        //public string GetColor() { return color; }
        //public void SetColor(string color) { this.color = color; }

        public DogWithProperties(string name, int weight = -1)
        {
            Name = name;
            this.weight = weight;
            Color = "Uninitialized";
        }

        public DogWithProperties() : this("Unknown") { }

        public override string ToString()
        {
            //return "Name: " + name + " Weight: " + weight;
            //return string.Format("Name: {0}; Weight: {1}", name, weight);
            return $"Name: {Name}; Weight: {Weight}; Color: {Color}";
        }
    }
}
