﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9
{
    class Program
    {
        static void Main(string[] args)
        {
            //MediaStorageTester();
            MediaStorageUsingEventTester();
        }

        private static void MediaStorageTester()
        {
            MediaStorage myMedia = new MediaStorage();
            AudioPlayer myAudioPlayer = new AudioPlayer();
            VideoPlayer myVideoPlayer = new VideoPlayer();

            MediaStorage.PlayMedia audioPlayer = myAudioPlayer.PlayAudio;
            MediaStorage.PlayMedia videoPlayer = myVideoPlayer.PlayVideo;

            myMedia.ReportResult(audioPlayer);
            myMedia.ReportResult(videoPlayer);
        }

        private static void MediaStorageUsingEventTester()
        {
            MediaStorageUsingEvent myMedia = new MediaStorageUsingEvent();
            AudioPlayerUsingEvent myAudioPlayer = new AudioPlayerUsingEvent(myMedia);
            VideoPlayerUsingEvent myVideoPlayer = new VideoPlayerUsingEvent(myMedia);

            myMedia.PlayAll();
        }
    }
}
