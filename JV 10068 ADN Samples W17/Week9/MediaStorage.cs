﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9
{
    class MediaStorage
    {
        public delegate int PlayMedia();

        public void ReportResult(PlayMedia playMedia)
        {
            if (playMedia() == 0)
                Console.WriteLine("Success!");
            else
                Console.WriteLine("Failed!");
        }
    }
}
