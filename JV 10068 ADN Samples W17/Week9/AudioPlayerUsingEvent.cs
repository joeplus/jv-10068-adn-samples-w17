﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9
{
    class AudioPlayerUsingEvent
    {
        public AudioPlayerUsingEvent(MediaStorageUsingEvent mediaStorage)
        {
            mediaStorage.playMediaEvent += PlayAudio;
        }

        public int PlayAudio()
        {
            Console.WriteLine("AudioPlayer.PlayAudio");
            return 0;
        }
    }
}
