﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9
{
    class VideoPlayerUsingEvent
    {
        public VideoPlayerUsingEvent(MediaStorageUsingEvent mediaStorage)
        {
            mediaStorage.playMediaEvent += PlayVideo;
        }

        public int PlayVideo()
        {
            Console.WriteLine("VideoPlayer.PlayVideo");
            return 1;
        }
    }
}
