﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9
{
    class MediaStorageUsingEvent
    {
        public delegate int PlayMedia();
        public event PlayMedia playMediaEvent;

        public void PlayAll()
        {
            int result;
            if (playMediaEvent != null)
                result = playMediaEvent();
        }
    }
}
