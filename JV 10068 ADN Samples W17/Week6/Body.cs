﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Body: IStorable
    {
        public int Status
        {
            get; set;
        }

        public bool Read()
        {
            Console.WriteLine("Body.Read");
            return true;
        }

        public bool Write(object o)
        {
            Console.WriteLine("Body.Write");
            if (o != null)
                return true;
            else
                return false;
        }
    }
}
