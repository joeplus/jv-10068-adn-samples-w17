﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Document : Note, ICompressible
    {
        public Document() : base() { }

        public void Compress()
        {
            Console.WriteLine("Compress");
        }

        public void Decompress()
        {
            Console.WriteLine("Decompress");
        }
    }
}
