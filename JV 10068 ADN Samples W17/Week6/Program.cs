﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Program
    {
        static void Main(string[] args)
        {
            //NoteTester();
            AllNoteTester();
        }

        private static void AllNoteTester()
        {
            Note[] myNotes = { new Note(),
                               new Document(),
                               new Note(),
                               new Document() };

            foreach (Note myNote in myNotes)
            {
                if (myNote is ICompressible)
                {
                    ICompressible myCompressibleNote = myNote as ICompressible;
                    myCompressibleNote.Compress();
                }
                myNote.Write(myNote);
            }
        }

        private static void NoteTester()
        {
            IStorable myVersion = new Version();
            IStorable myHeader = new Header();
            IStorable myBody = new Body();

            Note myNote = new Note(myVersion, myHeader, myBody);
            myNote.Write(myNote);
            myNote.Read();
        }
    }
}
