﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Note : IStorable
    {
        private static int count;

        public int ID { get; set;}
        public string Path { get; set; }
        public string FileName { get; set; }
        public string Author { get; set; }
        public int Size { get; set; }

        private IStorable myVersion;
        private IStorable myHeader;
        private IStorable myBody;

        //private Version myVersion;
        //private Header myHeader;
        //private Body myBody;


        public Note() : this(new Version(), new Header(), new Body()) 
        {
        }

        //public Note(Version theVersion, Header theHeader, Body theBody)
        //{
        //    myVersion = theVersion;
        //    myHeader = theHeader;
        //    myBody = theBody;
        //}

        public Note(IStorable theVersion, IStorable theHeader, IStorable theBody)
        {
            myVersion = theVersion;
            myHeader = theHeader;
            myBody = theBody;
            ID = ++count;
        }


        public int Status
        {
            get; set;
        }

        public bool Read()
        {
            Console.WriteLine("Note.Read");
            if (myVersion is IStorable)  // not necessary (always true) but for demonstration purposes
                myVersion.Read();
            myHeader.Read();
            myBody.Read();
            return true;
        }

        public bool Write(object o)
        {
            Console.WriteLine($"Note.Write ID: {ID}");
            if (o != null)
            {
                myVersion.Write(o);
                myHeader.Write(o);
                myBody.Write(o);
                return true;
            }
            else
                return false;
        }
    }
}
