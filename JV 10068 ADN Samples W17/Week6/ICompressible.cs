﻿namespace Week6
{
    internal interface ICompressible
    {
        void Compress();
        void Decompress();
    }
}