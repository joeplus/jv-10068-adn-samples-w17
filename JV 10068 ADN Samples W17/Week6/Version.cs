﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Version: IStorable
    {
    public int Status
    {
        get; set;
    }

    public bool Read()
    {
        Console.WriteLine("Version.Read");
        return true;
    }

    public bool Write(object o)
    {
        Console.WriteLine("Version.Write");
        if (o != null)
            return true;
        else
            return false;
    }
  }
}
