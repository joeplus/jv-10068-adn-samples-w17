﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week6
{
    class Header : IStorable
    {
        public int Status
        {
            get; set;
        }

        public bool Read()
        {
            Console.WriteLine("Header.Read");
            return true;
        }

        public bool Write(object o)
        {
            Console.WriteLine("Header.Write");
            if (o != null)
                return true;
            else
                return false;
        }
    }
}
