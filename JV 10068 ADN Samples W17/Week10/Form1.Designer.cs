﻿namespace Week10
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GreetingLbl = new System.Windows.Forms.Label();
            this.GreetingBtn = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.TimerToggleBtn = new System.Windows.Forms.Button();
            this.GreetingTmr = new System.Windows.Forms.Timer(this.components);
            this.SubTotalLbl = new System.Windows.Forms.Label();
            this.TaxLbl = new System.Windows.Forms.Label();
            this.SubtotalTB = new System.Windows.Forms.TextBox();
            this.TaxRateNUD = new System.Windows.Forms.NumericUpDown();
            this.TaxAmountLbl = new System.Windows.Forms.Label();
            this.TotalLbl = new System.Windows.Forms.Label();
            this.SubmitBtn = new System.Windows.Forms.Button();
            this.ClearBtn = new System.Windows.Forms.Button();
            this.AppEP = new System.Windows.Forms.ErrorProvider(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxRateNUD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppEP)).BeginInit();
            this.SuspendLayout();
            // 
            // GreetingLbl
            // 
            this.GreetingLbl.AutoSize = true;
            this.GreetingLbl.Location = new System.Drawing.Point(6, 19);
            this.GreetingLbl.Name = "GreetingLbl";
            this.GreetingLbl.Size = new System.Drawing.Size(40, 17);
            this.GreetingLbl.TabIndex = 0;
            this.GreetingLbl.Text = "Hello";
            // 
            // GreetingBtn
            // 
            this.GreetingBtn.Location = new System.Drawing.Point(6, 46);
            this.GreetingBtn.Name = "GreetingBtn";
            this.GreetingBtn.Size = new System.Drawing.Size(105, 33);
            this.GreetingBtn.TabIndex = 1;
            this.GreetingBtn.Text = "Greeting";
            this.GreetingBtn.UseVisualStyleBackColor = true;
            this.GreetingBtn.Click += new System.EventHandler(this.GreetingBtn_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(309, 258);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.TimerToggleBtn);
            this.tabPage1.Controls.Add(this.GreetingBtn);
            this.tabPage1.Controls.Add(this.GreetingLbl);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(301, 229);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ex. 10.1&2";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.ClearBtn);
            this.tabPage2.Controls.Add(this.SubmitBtn);
            this.tabPage2.Controls.Add(this.TotalLbl);
            this.tabPage2.Controls.Add(this.TaxAmountLbl);
            this.tabPage2.Controls.Add(this.TaxRateNUD);
            this.tabPage2.Controls.Add(this.SubtotalTB);
            this.tabPage2.Controls.Add(this.TaxLbl);
            this.tabPage2.Controls.Add(this.SubTotalLbl);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(301, 229);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ex. 10.3";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // TimerToggleBtn
            // 
            this.TimerToggleBtn.Location = new System.Drawing.Point(126, 46);
            this.TimerToggleBtn.Name = "TimerToggleBtn";
            this.TimerToggleBtn.Size = new System.Drawing.Size(105, 33);
            this.TimerToggleBtn.TabIndex = 2;
            this.TimerToggleBtn.Text = "Timer Toggle";
            this.TimerToggleBtn.UseVisualStyleBackColor = true;
            this.TimerToggleBtn.Click += new System.EventHandler(this.TimerToggleBtn_Click);
            // 
            // GreetingTmr
            // 
            this.GreetingTmr.Interval = 1000;
            this.GreetingTmr.Tick += new System.EventHandler(this.GreetingTmr_Tick);
            // 
            // SubTotalLbl
            // 
            this.SubTotalLbl.AutoSize = true;
            this.SubTotalLbl.Location = new System.Drawing.Point(6, 21);
            this.SubTotalLbl.Name = "SubTotalLbl";
            this.SubTotalLbl.Size = new System.Drawing.Size(64, 17);
            this.SubTotalLbl.TabIndex = 0;
            this.SubTotalLbl.Text = "Subtotal:";
            // 
            // TaxLbl
            // 
            this.TaxLbl.AutoSize = true;
            this.TaxLbl.Location = new System.Drawing.Point(35, 52);
            this.TaxLbl.Name = "TaxLbl";
            this.TaxLbl.Size = new System.Drawing.Size(35, 17);
            this.TaxLbl.TabIndex = 1;
            this.TaxLbl.Text = "Tax:";
            // 
            // SubtotalTB
            // 
            this.SubtotalTB.Location = new System.Drawing.Point(76, 18);
            this.SubtotalTB.Name = "SubtotalTB";
            this.SubtotalTB.Size = new System.Drawing.Size(120, 23);
            this.SubtotalTB.TabIndex = 2;
            this.SubtotalTB.Tag = "Invalid subtotal entered";
            // 
            // TaxRateNUD
            // 
            this.TaxRateNUD.DecimalPlaces = 2;
            this.TaxRateNUD.Increment = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            this.TaxRateNUD.Location = new System.Drawing.Point(76, 50);
            this.TaxRateNUD.Maximum = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.TaxRateNUD.Name = "TaxRateNUD";
            this.TaxRateNUD.Size = new System.Drawing.Size(120, 23);
            this.TaxRateNUD.TabIndex = 3;
            // 
            // TaxAmountLbl
            // 
            this.TaxAmountLbl.AutoSize = true;
            this.TaxAmountLbl.Location = new System.Drawing.Point(76, 127);
            this.TaxAmountLbl.Name = "TaxAmountLbl";
            this.TaxAmountLbl.Size = new System.Drawing.Size(0, 17);
            this.TaxAmountLbl.TabIndex = 4;
            // 
            // TotalLbl
            // 
            this.TotalLbl.AutoSize = true;
            this.TotalLbl.Location = new System.Drawing.Point(76, 163);
            this.TotalLbl.Name = "TotalLbl";
            this.TotalLbl.Size = new System.Drawing.Size(0, 17);
            this.TotalLbl.TabIndex = 5;
            // 
            // SubmitBtn
            // 
            this.SubmitBtn.Location = new System.Drawing.Point(38, 79);
            this.SubmitBtn.Name = "SubmitBtn";
            this.SubmitBtn.Size = new System.Drawing.Size(105, 33);
            this.SubmitBtn.TabIndex = 6;
            this.SubmitBtn.Text = "Submit";
            this.SubmitBtn.UseVisualStyleBackColor = true;
            this.SubmitBtn.Click += new System.EventHandler(this.SubmitBtn_Click);
            // 
            // ClearBtn
            // 
            this.ClearBtn.Location = new System.Drawing.Point(167, 79);
            this.ClearBtn.Name = "ClearBtn";
            this.ClearBtn.Size = new System.Drawing.Size(105, 33);
            this.ClearBtn.TabIndex = 7;
            this.ClearBtn.Text = "Clear";
            this.ClearBtn.UseVisualStyleBackColor = true;
            this.ClearBtn.Click += new System.EventHandler(this.ClearBtn_Click);
            // 
            // AppEP
            // 
            this.AppEP.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 274);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "ADN W17 Week10";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaxRateNUD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppEP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label GreetingLbl;
        private System.Windows.Forms.Button GreetingBtn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button TimerToggleBtn;
        private System.Windows.Forms.Timer GreetingTmr;
        private System.Windows.Forms.NumericUpDown TaxRateNUD;
        private System.Windows.Forms.TextBox SubtotalTB;
        private System.Windows.Forms.Label TaxLbl;
        private System.Windows.Forms.Label SubTotalLbl;
        private System.Windows.Forms.Button ClearBtn;
        private System.Windows.Forms.Button SubmitBtn;
        private System.Windows.Forms.Label TotalLbl;
        private System.Windows.Forms.Label TaxAmountLbl;
        private System.Windows.Forms.ErrorProvider AppEP;
    }
}

