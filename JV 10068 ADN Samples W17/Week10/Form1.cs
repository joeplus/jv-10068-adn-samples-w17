﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week10
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GreetingBtn_Click(object sender, EventArgs e)
        {
            if (GreetingLbl.Text == "Hello")
                GreetingLbl.Text = "Goodbye";
            else
                GreetingLbl.Text = "Hello";
        }

        private void GreetingTmr_Tick(object sender, EventArgs e)
        {
            GreetingBtn_Click(sender, e);
        }

        private void TimerToggleBtn_Click(object sender, EventArgs e)
        {
            GreetingTmr.Enabled = !GreetingTmr.Enabled;
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            decimal subtotal;
            bool result = decimal.TryParse(SubtotalTB.Text, out subtotal);

            if (result)
            {
                decimal tax = subtotal * TaxRateNUD.Value / 100;
                decimal total = subtotal + tax;

                TaxAmountLbl.Text = "Tax: " + tax.ToString("C");
                TotalLbl.Text = "Total: " + total.ToString("C");
                AppEP.SetError(SubtotalTB, string.Empty);
            }
            else
                AppEP.SetError(SubtotalTB, SubtotalTB.Tag.ToString());
        }

        private void ClearBtn_Click(object sender, EventArgs e)
        {
            TaxAmountLbl.Text = TotalLbl.Text = SubtotalTB.Text = string.Empty;
            AppEP.SetError(SubtotalTB, string.Empty);
        }
    }
}
