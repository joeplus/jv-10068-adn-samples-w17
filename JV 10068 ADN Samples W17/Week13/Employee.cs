﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Week13
{
    [Table(Name = "Employees")]
    class Employee
    {
        [Column] public int EmployeeID { get; set; }
        [Column] public string FirstName { get; set; }
        [Column] public string LastName { get; set; }
    }
}
