﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;

namespace Week13
{
    class Program
    {
        static void Main(string[] args)
        {
            //LinqToSqlTest();
            DesignerLinqToSqlTest();
        }

        private static void DesignerLinqToSqlTest()
        {
            OrderClassesDataContext ordersDC = new OrderClassesDataContext();

            var orderDetails = from order in ordersDC.Order_Details
                         where order.OrderID >= 10250 && order.OrderID <= 10255
                         select order;
            foreach (var od in orderDetails)
                Console.WriteLine("{0} {1} {2}", od.OrderID,
                                  od.Quantity, od.Product.ProductName);

            Console.WriteLine("***Using Lambda***");
            orderDetails = ordersDC.Order_Details.
                      Where(od => od.OrderID >= 10250 && od.OrderID <= 10255);
            foreach (var od in orderDetails)
                Console.WriteLine("{0} {1} {2}", od.OrderID,
                                  od.Quantity, od.Product.ProductName);

        }

        private static void LinqToSqlTest()
        {
            DataContext db = new DataContext("Data Source = .\\SQLExpress;" + 
                                  "Initial Catalog = Northwind; Integrated Security = True");
            Table<Employee> employees = db.GetTable<Employee>();

            IEnumerable<Employee> emps = from emp in employees select emp;
            foreach (Employee emp in emps)
                Console.WriteLine("{0}, {1}, {2}", emp.EmployeeID, emp.FirstName, 
                                    emp.LastName);

            Console.WriteLine("***Using Lambda***");
            emps = employees.Select(e=>e);
            foreach (Employee emp in emps)
                Console.WriteLine("{0}, {1}, {2}", emp.EmployeeID, emp.FirstName,
                                    emp.LastName);
        }
    }
}
