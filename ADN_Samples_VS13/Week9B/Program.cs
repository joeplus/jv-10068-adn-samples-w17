﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9A
{
    class Program
    {
        static void Main(string[] args)
        {
            ClockTester();
        }

        private static void ClockTester()
        {
            Clock myClock = new Clock(1000);
            Logger myLogger = new Logger(myClock);

            while (Console.ReadLine() != "Q")
                ;
        }
    }
}
