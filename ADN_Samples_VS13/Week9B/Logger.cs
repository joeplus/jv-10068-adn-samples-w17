﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week9A
{
    class Logger
    {
        public Logger(Clock myClock)
        {
            myClock.ClockTick += ClockTickHandler;
            myClock.CustomClockTick += CustomClockTickHandler;
        }

        private void ClockTickHandler(object sender, EventArgs e)
        {
            Console.WriteLine("Logger ClockTickHandler");
        }

        private void CustomClockTickHandler(object sender, ClockEventArgs e)
        {
            if (e.ticks % 20 == 0)
                Console.WriteLine("Logger CustomClockTickHandler");
        }
    }
}
