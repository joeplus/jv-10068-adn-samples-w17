﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Week9A
{
    class Clock
    {
        private int ticks;
        private System.Timers.Timer myTimer;

        public Clock(int milliseconds)
        {
            myTimer = new Timer();
            myTimer.Interval = milliseconds;
            myTimer.Elapsed += MyTimerElapsedHandler;
            myTimer.Start();
        }

        public void MyTimerElapsedHandler(object sender, ElapsedEventArgs e)
        {
            if (++ticks % 10 == 0)
            {
                OnClockTick();
                OnCustomClockTick();
            }
        }

        public event EventHandler ClockTick;

        //public void EventHandler(object sender, EventArgs e)
        public void OnClockTick()
        {
            if (ClockTick != null)
                ClockTick(this, EventArgs.Empty);
        }

        public void OnCustomClockTick()
        {
            if (CustomClockTick != null)
                CustomClockTick(this, new ClockEventArgs(ticks));
        }

        public event EventHandler<ClockEventArgs> CustomClockTick;
    }

    public class ClockEventArgs : EventArgs
    {
        public int ticks;

        public ClockEventArgs(int ticks)
        {
            this.ticks = ticks;
        }
    }
}
