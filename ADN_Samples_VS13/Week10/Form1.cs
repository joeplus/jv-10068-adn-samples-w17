﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week10
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            decimal result;
            //Resource1.Culture = new System.Globalization.CultureInfo("fr-CA");
            if (decimal.TryParse(TotalTB.Text, out result))
                MessageBox.Show("Hurray");
            else
                MessageBox.Show(Resource1.TotalFormatError);         
        }
    }
}
