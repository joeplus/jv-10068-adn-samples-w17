﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week7_VST13
{
    abstract class Vehicle
    {
        public abstract void Buy();
    }
}
