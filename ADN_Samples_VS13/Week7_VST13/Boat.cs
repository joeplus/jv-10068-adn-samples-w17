﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week7_VST13
{
    class Boat : Vehicle
    {
        public override void Buy()
        {
            Console.WriteLine("Congratulation you bought a new BOAT!");
        }
    }
}
