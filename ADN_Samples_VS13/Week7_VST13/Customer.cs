﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week7_VST13
{
    public class Customer : IComparable<Customer>
    {
        private static int count;
        public int ID { get; private set; }
        public string Phone { get; private set; }
        public string FName { get; private set; }
        public string LName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }

        public Customer(string phone, string fName, string lName = "Undefined",
                        string address = "Undefined", string city = "Undefined") {
                ID = ++count;
                Phone = phone;
                FName = fName;
                LName = lName;
                StreetAddress = address;
                City = city;
        }

        public int CompareTo(Customer other)
        {
            return FName.CompareTo(other.FName);
        }

        public override string ToString()
        {
            return string.Format("Phone: {0}, FirstName: {1}, LastName: {2}, City: {3}", 
                                  Phone, FName, LName, City);
        }
    }
}
