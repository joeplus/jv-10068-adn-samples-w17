﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week7_VST13
{
    class Program
    {
        private static Dictionary<string, Customer> ourCustomers;

        static void Main(string[] args)
        {
            //ListTester();
            //ListVehicleTester();
            CustomerTester();
        }

        private static void CustomerTester()
        {
            AddCustomers();
            //DisplayCustomers();
            DisplaySortedCustomers();
            FindCustomer();
        }

        private static void DisplaySortedCustomers()
        {
            List<Customer> customers = ourCustomers.Values.ToList();

            Console.WriteLine("Unsorted Customer List:");
            foreach (Customer customer in customers)
                Console.WriteLine(customer);

            customers.Sort();
            Console.WriteLine("Sorted Customer List:");
            foreach (Customer customer in customers)
                Console.WriteLine(customer);
        }

        private static void FindCustomer()
        {
            string phone = "";
            do
            {
                Console.WriteLine("Enter phone number (Q to Quit): ");
                phone = Console.ReadLine();
                if (phone == "Q")
                    break;

                if (ourCustomers.ContainsKey(phone))
                {
                    Customer customer = ourCustomers[phone];
                    Console.WriteLine(customer);
                }
                else
                    Console.WriteLine("Phone number not found!");
            } while (true);
        }

        private static void DisplayCustomers()
        {
            foreach (Customer customer in ourCustomers.Values)
                Console.WriteLine(customer);
        }

        private static void AddCustomers()
        {
            ourCustomers = new Dictionary<string,Customer>();

            // some time later we add some customers
            ourCustomers.Add("9055751212", new Customer("9055751212", "Kayla"));
            ourCustomers.Add("9055751213", new Customer("9055751213", "Ali"));
            ourCustomers.Add("9055751214", new Customer("9055751214", "Paramjit"));
            ourCustomers.Add("9055751215", new Customer("9055751215", "John"));
            ourCustomers.Add("9055751216", new Customer("9055751215", "Sue"));
        }

        private static void ListVehicleTester()
        {
            List<Vehicle> myVehicles = new List<Vehicle>();
            string selectedVehicle = string.Empty;

            do
            {
                Console.WriteLine("Enter vehicle you bought (Car or Boat, Q to quit)");
                selectedVehicle = Console.ReadLine();
                if (selectedVehicle == "Car")
                    myVehicles.Add(new Car());
                else if (selectedVehicle == "Boat")
                    myVehicles.Add(new Boat());
            } while (selectedVehicle != "Q");

            foreach (Vehicle theVehicle in myVehicles)
            {
                theVehicle.Buy();
                Console.WriteLine(theVehicle);
            }
        }

        private static void ListTester()
        {
            List<string> myFavourites = new List<string>();
            string selectedCourse = string.Empty;

            do {
                Console.WriteLine("Enter favourite CSAIT course (Q to quit)");
                selectedCourse = Console.ReadLine();
                myFavourites.Add(selectedCourse);
            } while (selectedCourse != "Q");
            myFavourites.Remove("Q");

            int count = 0;
            for (int i = 0; i < myFavourites.Count - 1; i++)
            {
                if (myFavourites[i] == "PHP")
                    myFavourites[i] = "Advanced PHP";
            }

            myFavourites.Sort();

            foreach (string favourite in myFavourites)
            {
                if (favourite == "Advanced .NET")
                    count++;
                Console.WriteLine(favourite);
            }

        }
    }
}
